import React, { useMemo } from 'react';
import * as ST from './styled';
import { useLocation } from 'react-router-dom';

interface IMenuItem {
  path: string,
  label: string,
}

const menuItems: IMenuItem[] = [
  {
    path: 'task-one',
    label: 'Первое задание',
  },
  {
    path: 'task-two',
    label: 'Второе задание',
  },
  {
    path: 'task-three',
    label: 'Третье задание',
  }
];

const Header: React.FC = () => {
  const location = useLocation();
  const { pathname } = location;

  const currentPath = useMemo(() => {
    return pathname.substring(1).split("/")[0];
  }, [pathname]);

  return (
    <ST.AppContainer>
      {
        menuItems.map(menuItem => (<ST.LinkText
          key={menuItem.path}
          to={`/${menuItem.path}`}
          isSelected={menuItem.path === currentPath}
        >
          {menuItem.label}
        </ST.LinkText>))
      }
    </ST.AppContainer>
  );
}

export default Header;