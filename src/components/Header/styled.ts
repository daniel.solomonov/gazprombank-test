import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const AppContainer = styled.div`
    padding: 10px;
    background-color: lightblue;
    height: auto;
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    box-shadow: gray 0px 0px 15px;
    gap: 10px;
`;

export const LinkText = styled(Link)<{ isSelected?: boolean }>`
    text-decoration: none;
    color: inherit;
    font-size: 14px;
    font-weight: 500;
    padding: 5px 10px;
    border: solid 1px blue;
    border-radius: 5px;
    background-color: ${({ isSelected }) => isSelected ? 'lightgreen' : 'transparent'};

    transition: all 0.2s ease;
    box-shadow: black 0px 0px 2px;
    &:hover {
        transform: scale(1.03) translate(0, 2px);
        transition: all 0.2s ease;
        box-shadow: blue 0px 0px 4px;
    }
`;
