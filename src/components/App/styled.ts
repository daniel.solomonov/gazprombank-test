import styled from 'styled-components';

export const AppContainer = styled.div`
    text-align: center;
    background-color: #c8daff;
    min-height: 100vh;
`;

export const ContentWrapper = styled.div`
    padding: 15px 10px;
    border-radius: 10px;
    overflow: auto;
`;
