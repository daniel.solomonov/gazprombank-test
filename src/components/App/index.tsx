import React from 'react';
import { Routes, Route } from 'react-router-dom';
import * as ST from './styled';
import Header from '../Header';
import TaskOne from '../TaskOne';
import TaskTwo from '../TaskTwo';
import TaskThree from '../TaskThree';

const App: React.FC = () => {
    return (
        <ST.AppContainer>
            <Header />
            <ST.ContentWrapper>
                <Routes>
                    <Route path={'/task-one'} element={<TaskOne />} />
                    <Route path={'/task-two'} element={<TaskTwo />} />
                    <Route path={'/task-three'} element={<TaskThree />} />
                    <Route path="/" element={<>Выберите один из пунктов меню</>} />
                </Routes>
            </ST.ContentWrapper>
        </ST.AppContainer>
    );
}

export default App;
