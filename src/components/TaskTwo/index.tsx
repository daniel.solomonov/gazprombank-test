import React, { useCallback, useState } from 'react';
import axios from 'axios';
import ServiceCard, { IService } from '../../templates/ServiceCard';
import ServiceDetails, { IDetails } from '../../templates/ServiceDetails';
import * as ST from './styled';

const servicesUrl = 'http://localhost:7070/api/services';

const TaskTwo: React.FC = () => {
  const [services, setServices] = useState<IService[]>();
  const [details, setDetails] = useState<IDetails>();
  const [currentId, setCurrentId] = useState<string>();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  const requestServices = useCallback(() => {
    setIsLoading(true);
    if (!services) {
      axios(servicesUrl)
        .then(response => {
          const { statusText, data } = response;
          if (statusText === 'OK' && data) {
            setServices(data);
            setIsError(false);
            console.log('data', data);
          } else {
            setIsError(true);
            console.log('error', response);
          }
        })
        .catch(err => {
          setIsError(true);
          console.log('err', err);
        })
        .finally(() => setIsLoading(false));
    }
  }, [services]);

  const requestDetail = useCallback((id?: string) => {
    if (currentId || id) {
      const serviceId = id ? id : currentId;
      setIsLoading(true);
      axios(servicesUrl + `/${serviceId}`)
        .then(response => {
          const { statusText, data } = response;
          if (statusText === 'OK' && data) {
            setDetails(data);
            setIsError(false);
            console.log('data', data);
          } else {
            setIsError(true);
            console.log('error', response);
          }
        })
        .catch(err => {
          setIsError(true);
          console.log('err', err);
        })
        .finally(() => setIsLoading(false));
    }
  }, [currentId]);

  const [requestType, setRequestType] = useState<number>(1);

  const onRequestServices = useCallback(() => {
    setRequestType(1);
    requestServices();
  }, [requestServices]);

  const onRequestDetail = useCallback((id: string) => {
    setRequestType(2);
    setCurrentId(id);
    requestDetail(id);
  }, [requestDetail]);

  const onRequestRepeat = useCallback(() => {
    if (requestType === 1) {
      requestServices();
    } else {
      requestDetail();
    }
  }, [requestType, requestServices, requestDetail]);

  return (
    <ST.Container>
      {isLoading
        ? <ST.Spinner />
        : isError
          ? <>
            Произошла ошибка!
            <ST.Button
              onClick={onRequestRepeat}
            >
              Повторить запрос
            </ST.Button>
          </>
          : null
      }
      <ST.ContentWrapper>
        {services
          ? <ST.Services>
            {
              services.map(service => (<ServiceCard
                key={service.id}
                {...service}
                onClick={onRequestDetail}
              />))
            }
          </ST.Services>
          : !isError && !isLoading
            ? <ST.Button onClick={onRequestServices}>Загрузить сервисы</ST.Button>
            : null
        }
        {details
          && <ServiceDetails
            {...details}
          />
        }
      </ST.ContentWrapper>
    </ST.Container>
  );
}

export default TaskTwo;