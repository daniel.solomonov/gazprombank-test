import styled, { keyframes } from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    gap: 10px;
`;

export const Button = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    align-self: center;
    background-color: pink;
    box-shadow: black 0px 0px 3px;
    height: 30px;
    width: 200px;
    cursor: pointer;
    border-radius: 5px;
`;

export const ContentWrapper = styled.div`
    display: flex;
    flex-direction: row;
    gap: 10px;
`;

export const Services = styled.div`
    display: flex;
    flex-direction: column;
    gap: 10px;
`;

export const Detail = styled.div`
    display: flex;
    background-color: pink;
    border: solid 10px red;
    height: 400px;
    width: 200px;
`;

const rotation = keyframes`
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
`;

export const Spinner = styled.span`
    align-self: center;
    width: 30px;
    height: 30px;
    border: 3px solid white;
    border-bottom-color: green;
    border-radius: 50%;
    display: inline-block;
    box-sizing: border-box;
    animation: ${rotation} 1s linear infinite;
`;
