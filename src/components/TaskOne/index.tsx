import React from 'react';
import Card, { ICard } from '../../templates/Card';
import * as ST from './styled';

const cards: ICard[] = [
  {
    header: "Заголовок 1",
    options: [
      "элемент списка 1",
      "элемент списка 2",
      "элемент списка 3",
    ],
    text: "какой-то текст 1 текст какой-то 1 какой-то"
  },
  {
    header: "Заголовок 2",
    options: [
      "элемент списка 1",
      "элемент списка 2",
      "элемент списка 3",
    ],
    text: "какой-то текст 2 текст какой-то 2 какой-то"
  },
  {
    header: "Заголовок 3",
    options: [
      "элемент списка 1",
      "элемент списка 2",
      "элемент списка 3",
    ],
    text: "какой-то текст 3 текст какой-то 3 какой-то"
  },
  {
    header: "Заголовок 4",
    options: [
      "элемент списка 1",
      "элемент списка 2",
      "элемент списка 3",
    ],
    text: "какой-то текст 4 текст какой-то 4 какой-то/ какой-то текст 4 текст"
  },
];

const TaskOne: React.FC = () => {

  return (
    <ST.ContentWrapper>
      {
        cards.map(card => (<Card
          key={card.header}
          {...card}
        />))
      }
    </ST.ContentWrapper>
  );
}

export default TaskOne;