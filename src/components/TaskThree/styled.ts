import styled from 'styled-components';

export const AppContainer = styled.div`
    //
`;

export const DateControlWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    padding: 10px;
    gap: 10px;
`;

export const MonthsRows = styled.div`
    display: flex;
    flex-direction: column;
    width: fit-content;
    border: solid 2px red;
`;

export const Button = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    align-self: center;
    background-color: pink;
    box-shadow: black 0px 0px 3px;
    height: 30px;
    width: 200px;
    cursor: pointer;
    border-radius: 5px;
`;
