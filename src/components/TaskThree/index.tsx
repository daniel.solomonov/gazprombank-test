import React, { useCallback, useMemo, useState } from 'react';
import moment from 'moment';
import MonthsRow from './components/MonthsRow';
import * as ST from './styled';
import SelectOption from '../../templates/SelectOption/SelectOption';
import RadioButton from '../../templates/RadioButton/RadioButton';
import Month from './components/Month';
import Overlay from '../../templates/Overlay';
import type { RootState } from '../../store/redux_store';
import { useSelector, useDispatch } from 'react-redux';
import { addEvent, removeEvent } from '../../store/features/eventsSlice';
import ListEvent from './components/ListEvent';
import AddEvent from './components/AddEvent';

interface IGetMonth {
  year: string,
  monthIndex: string,
}

export const monthNames = [
  'Январь',
  'Февраль',
  'Март',
  'Апрель',
  'Май',
  'Июнь',
  'Июль',
  'Август',
  'Сентябрь',
  'Октябрь',
  'Ноябрь',
  'Декабрь',
];

const variantsYear = ['2020', '2021', '2022', '2023', '2024', '2025', '2026'];
const variantsShow = ['Месяц', 'Год'];

const TaskThree: React.FC = () => {
  const currentYear = moment().year();
  const currentMonth = moment().month();
  const currentDay = moment().date();
  const [selectedYear, setSelectedYear] = useState<string>(moment().year().toString());
  const onYearSelect = useCallback((event: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedYear(event.target.value);
  }, []);

  const [selectedMonth, setSelectedMonth] = useState<string>(monthNames[8]);
  const onMonthSelect = useCallback((event: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedMonth(event.target.value);
  }, []);

  const [selectedShow, setSelectedShow] = useState<string>(variantsShow[1]);
  const onShowSelect = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedShow(event.target.value);
  }, []);

  const getMonth = useCallback(({ year, monthIndex }: IGetMonth) => {
    const currentMonth = moment(`${year}-${monthIndex}-01`, 'YYYY-MM-DD');

    const currentMonthDays = currentMonth.daysInMonth();
    const dayFrom = ((currentMonth.weekday() - 1) + 7) % 7;
    const restDays = (currentMonthDays + dayFrom) % 7;
    const dayEnd = currentMonthDays + (restDays > 0 ? (7 - restDays) : 0);

    const currentMonthDates = [];
    for (let i = -dayFrom; i < dayEnd; i++) {
      const currentMonthDay = currentMonth.clone().add(i, 'days').format("D");
      currentMonthDates.push(currentMonthDay);
    }
    const size = 7;
    const subarray = [];
    for (let i = 0; i < Math.ceil(currentMonthDates.length / size); i++) {
      subarray[i] = currentMonthDates.slice((i * size), (i * size) + size);
    }
    return subarray;
  }, []);

  const monthRows: string[][][][] = useMemo(() => {
    const result: string[][][][] = [];
    let monthsRow: string[][][] = [];
    for (let i = 1; i < 12 + 1; i++) {
      monthsRow.push(getMonth({
        year: selectedYear,
        monthIndex: '' + i,
      }));
      if (i % 4 === 0) {
        result.push(monthsRow);
        monthsRow = [];
      }
    }
    return result;
  }, [selectedYear, getMonth]);

  const monthIndex = monthNames.indexOf(selectedMonth);

  const [isOverlayOpened, setIsOverlayOpened] = useState<boolean>(false);

  const [clickedDate, setClickedDate] = useState<number[]>();

  const Callback = useCallback((clickedDate: number[]) => {
    setClickedDate(clickedDate);
    setIsOverlayOpened(true);
    console.log(`day: ${clickedDate[0]}, month: ${clickedDate[1]}, year: ${clickedDate[2]}`);
  }, []);

  //
  const [isListEventOpened, setIsListEventOpened] = useState<boolean>(false);
  const dispatch = useDispatch();
  const events = useSelector((state: RootState) => state.events);
  const clickedDateStr = clickedDate ? `${clickedDate[0]}-${clickedDate[1]}-${clickedDate[2]}` : '';
  const selectedDateEvents = clickedDate
    ? events.filter(({ date }) => date === clickedDateStr)
    : [];
  console.log(events);
  // console.log(selectedDateEvents);
  //

  return (
    <ST.AppContainer>
      <div>
        <button
          onClick={() => dispatch(removeEvent({
            date: '01-01-2023',
            timeStart: 'timeStart3',
          }))}
        >
          removeEvent
        </button>
      </div>
      {/*  */}
      <ST.DateControlWrapper>
        <SelectOption
          variants={variantsYear}
          radioHandler={onYearSelect}
          selected={selectedYear}
        />
        <SelectOption
          variants={monthNames}
          radioHandler={onMonthSelect}
          selected={selectedMonth}
        />
        <RadioButton
          variants={variantsShow}
          radioHandler={onShowSelect}
          selected={selectedShow}
        />
      </ST.DateControlWrapper>
      {selectedShow === variantsShow[1]
        ? <ST.MonthsRows>
          {monthRows &&
            monthRows.map((monthRow, monthRowIndex) => (<MonthsRow
              key={`MonthsRow_monthRow_${monthRow[0]}_index_${monthRowIndex}`}
              monthRow={monthRow}
              monthRowIndex={monthRowIndex}
              year={+selectedYear}
              currentDate={[currentDay, currentMonth + 1, currentYear]}
              callback={Callback}
            />))
          }
        </ST.MonthsRows>
        : <Month
          key={`Month_month_${monthIndex}`}
          monthNumber={monthIndex}
          month={
            monthRows
            [Math.floor((monthIndex) / 4)]
            [monthIndex % 4]
          }
          monthIndex={monthIndex}
          year={+selectedYear}
          currentDate={[currentDay, currentMonth + 1, currentYear]}
          callback={Callback}
        />
      }
      {isOverlayOpened && clickedDate &&
        <Overlay
          onClose={() => setIsOverlayOpened(false)}
          label={`${clickedDate[0]} ${monthNames[clickedDate[1] - 1]} ${clickedDate[2]}`}
        >
          <>
            {/* {selectedDateEvents.length > 0 && <>
              <ST.Button
                onClick={() => setIsEventsListOpened(prev => !prev)}
              >
                Показать события
              </ST.Button>
              {isEventsListOpened && <ListEvent />}
            </>} */}
            <AddEvent
              clickedDateStr={clickedDateStr}
            />
          </>
        </Overlay>
      }
    </ST.AppContainer>
  );
}

export default TaskThree;
