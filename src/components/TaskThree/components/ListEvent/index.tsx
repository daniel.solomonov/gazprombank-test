import React from 'react';
import * as ST from './styled';

interface IListEvent {
  isOtherMonth?: boolean,
}

const ListEvent: React.FC<IListEvent> = ({ isOtherMonth }) => {

  return (
    <ST.Day
      onClick={() => { }}
    >
      {'ListEvent'}
    </ST.Day>
  );
}

export default ListEvent;
