import React from 'react';
import Month from '../Month';
import * as ST from './styled';

interface IMonthsRow {
  monthRow: string[][][],
  monthRowIndex: number,
  year: number,
  currentDate: number[],
  callback: (clickedDate: number[]) => void,
}

const MonthsRow: React.FC<IMonthsRow> = ({ monthRow, monthRowIndex, year, currentDate, callback }) => {

  return (
    <ST.MonthsRow>
      {
        monthRow.map((month, monthIndex) => {
          const monthNumber = (monthRowIndex * 4) + monthIndex;
          return (<Month
            key={`Month_month_${month[0]}_index_${monthIndex}`}
            monthNumber={monthNumber}
            month={month}
            monthIndex={monthIndex}
            year={year}
            currentDate={currentDate}
            callback={callback}
          />);
        })
      }
    </ST.MonthsRow>
  );
}

export default MonthsRow;
