import styled from 'styled-components';

export const Day = styled.div<{ isOtherMonth: boolean, isCurrentDateDay: boolean }>`
    display: flex;
    justify-content: center;
    align-items: center;
    border: solid 1px green;
    padding: 3px;
    width: 20px;
    height: 20px;
    font-size: 10px;
    font-weight: 700;
    cursor: pointer;
    background-color: ${({ isOtherMonth, isCurrentDateDay }) =>
        isCurrentDateDay ? 'red' : isOtherMonth ? 'silver' : 'lightgreen'};
`;
