import React, { useCallback } from 'react';
import * as ST from './styled';

interface IDay {
  isOtherMonth: boolean,
  day: number,
  month: number,
  year: number,
  currentDate: number[],
  callback: (clickedDate: number[]) => void,
}

const Day: React.FC<IDay> = ({ isOtherMonth, day, month, year, currentDate, callback }) => {

  const onDayClick = useCallback(() => {
    callback([day, month, year]);
  }, [callback, day, month, year]);

  const isCurrentDateDay =
    currentDate[0].toString() === day.toString()
    && currentDate[1].toString() === month.toString()
    && currentDate[2].toString() === year.toString();

  return (
    <ST.Day
      isOtherMonth={isOtherMonth}
      onClick={onDayClick}
      isCurrentDateDay={isCurrentDateDay}
    >
      {day}
    </ST.Day>
  );
}

export default Day;
