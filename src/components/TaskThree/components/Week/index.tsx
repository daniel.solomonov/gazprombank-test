import React from 'react';
import * as ST from './styled';
import Day from '../Day';

interface IWeek {
  week: string[],
  isFirstWeek: boolean,
  isLastWeek: boolean,
  monthNumber: number,
  year: number,
  currentDate: number[],
  callback: (clickedDate: number[]) => void,
}

const Week: React.FC<IWeek> = ({ week, isFirstWeek, isLastWeek, monthNumber, year, currentDate, callback }) => {

  return (
    <ST.Week>
      {week.map((day: any) => {
        const isPrevMonth = isFirstWeek && day > 20;
        const isNextMonth = isLastWeek && day < 10;
        const monthChangeNumber = isPrevMonth ? -1 : isNextMonth ? +1 : 0;
        const currentMonth = monthNumber + monthChangeNumber + 1;
        return (<Day
          key={`Day_day_${day}_week_${currentMonth}_${week}`}
          isOtherMonth={isPrevMonth || isNextMonth}
          day={day}
          month={currentMonth}
          year={year}
          currentDate={currentDate}
          callback={callback}
        />);
      })}
    </ST.Week>
  );
}

export default Week;
