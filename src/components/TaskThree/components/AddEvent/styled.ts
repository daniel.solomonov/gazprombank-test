import styled from 'styled-components';

export const AddEvent = styled.div`
    display: flex;
    flex-direction: column;
    gap: 7px;
    padding: 10px;
`;

export const Inputs = styled.div`
    display: flex;
    flex-direction: column;
    gap: 7px;
`;

export const Button = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    align-self: center;
    background-color: pink;
    box-shadow: black 0px 0px 3px;
    padding: 3px 10px;
    cursor: pointer;
    border-radius: 5px;
`;
