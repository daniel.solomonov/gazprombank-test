import React, { useCallback, useState } from 'react';
import * as ST from './styled';
import { useDispatch } from 'react-redux';
import { addEvent } from '../../../../store/features/eventsSlice';

interface IAddEvent {
  clickedDateStr: string,
}

const AddEvent: React.FC<IAddEvent> = ({ clickedDateStr }) => {
  const [isAddEventOpened, setIsAddEventOpened] = useState<boolean>(false);
  const [name, setName] = useState<string>();
  const [timeStart, setTimeStart] = useState<string>();
  const [timeEnd, setTimeEnd] = useState<string>();
  const [timeRemind, setTimeRemind] = useState<number>();

  const nameHandler = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  }, []);

  const timeStartHandler = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setTimeStart(event.target.value);
  }, []);

  const timeEndHandler = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setTimeEnd(event.target.value);
  }, []);

  const timeRemindHandler = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setTimeRemind(+event.target.value);
  }, []);

  const dispatch = useDispatch();

  const onAddEvent: any = useCallback(() => {
    if (name && timeStart && timeEnd && timeRemind) {
      dispatch(addEvent({
        date: clickedDateStr,
        event: {
          name,
          timeStart,
          timeEnd,
          timeRemind,
        },
      }));
      setName(undefined);
      setTimeStart(undefined);
      setTimeEnd(undefined);
      setTimeRemind(undefined);
    }
  }, [clickedDateStr, dispatch, name, timeEnd, timeRemind, timeStart]);

  return (
    <ST.AddEvent>
      <ST.Button
        onClick={() => setIsAddEventOpened(prev => !prev)}
      >
        Добавить событие
      </ST.Button>
      {isAddEventOpened &&
        <ST.Inputs>
          <input
            value={name}
            onChange={nameHandler}
            placeholder='Название'
          />
          <input
            value={timeStart}
            onChange={timeStartHandler}
            placeholder='Начало 10-00'
          />
          <input
            value={timeEnd}
            onChange={timeEndHandler}
            placeholder='Окончание 12-30'
          />
          <input
            value={timeRemind}
            type='number'
            onChange={timeRemindHandler}
            placeholder='Напоминание за 30 мин'
          />
          <ST.Button
            onClick={onAddEvent}
          >
            Сохранить
          </ST.Button>
        </ST.Inputs>
      }
    </ST.AddEvent>
  );
}

export default AddEvent;
