import React from 'react';
import { monthNames } from '../..';
import Week from '../Week';
import * as ST from './styled';

interface IMonth {
  monthNumber: number,
  month: string[][],
  monthIndex: number,
  year: number,
  currentDate: number[],
  callback: (clickedDate: number[]) => void,
}

const Month: React.FC<IMonth> = ({ monthNumber, month, monthIndex, year, currentDate, callback }) => {

  return (
    <ST.Month>
      {monthNames[monthNumber]}
      {
        month.map((week, index) => {
          const isFirstWeek = index === 0;
          const isLastWeek = month.length - 1 === index;
          return (<Week
            key={`Week_week_${week[0]}_index_${monthIndex}`}
            week={week}
            isFirstWeek={isFirstWeek}
            isLastWeek={isLastWeek}
            monthNumber={monthNumber}
            year={year}
            currentDate={currentDate}
            callback={callback}
          />);
        })
      }
    </ST.Month>
  );
}

export default Month;
