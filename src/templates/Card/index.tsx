import React from 'react';
import * as ST from './styled';

export interface ICard {
  header: string,
  options: string[],
  text: string,
}

const Card: React.FC<ICard> = ({ header, options, text }) => {

  return (
    <ST.ContentWrapper>
      <span>{header}</span>
      <ul>
        {
          options.map(option => (<li>{option}</li>))
        }
      </ul>
      <span>{text}</span>
    </ST.ContentWrapper>
  );
}

export default Card;