import * as ST from './styled';

interface IRadioButton {
  radioHandler: (event: React.ChangeEvent<HTMLInputElement>) => void,
  variants: string[],
  selected: string,
};

const RadioButton = ({ radioHandler, variants, selected }: IRadioButton) => {
  return (
    <ST.RadioButtonContainer>
      {
        variants.map((variant, index) => {
          const isFirst = index === 0;
          const isLast = index === variants.length - 1;
          return (<ST.RadioButtonVariant key={variant} selected={variant === selected}>
            <ST.RadioButtonInput
              type="radio"
              id={variant}
              value={variant}
              checked={variant === selected}
              onChange={radioHandler}
            />
            <label htmlFor={variant}>
              <ST.RadioButtonSpan
                selected={variant === selected}
                isFirst={isFirst}
                isLast={isLast}
              >
                {variant}
              </ST.RadioButtonSpan>
            </label>
          </ST.RadioButtonVariant>);
        })
      }
    </ST.RadioButtonContainer>
  );
};

export default RadioButton;
