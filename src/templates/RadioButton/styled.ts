import styled, { css } from 'styled-components';

export const RadioButtonContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-around;
`;

export const RadioButtonVariant = styled.div<{ selected: boolean }>`
    width: 100%;
    cursor: default;
    height: 26px;
`;

export const RadioButtonInput = styled.input`
    display: none;
`;

export const RadioButtonSpan = styled.span<{ selected: boolean, isFirst?: boolean, isLast?: boolean }>`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    width: 80px;
    height: 26px;
    cursor: pointer;
    background-color: silver;
    border-style: solid;
    border-width: 2px;
    border-color: ${({ selected }) => selected ? 'black' : 'silver'};
    ${({ isFirst, isLast }) => isFirst
        ? css`
            border-top-left-radius: 20px;
            border-bottom-left-radius: 20px;
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        `
        : isLast && css`
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
            border-top-right-radius: 20px;
            border-bottom-right-radius: 20px;
        `}
`;
