import styled from 'styled-components';

export const OverlayWrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    position: fixed;
    width: 100%;
    height: 100%;
    background-color: rgb(0, 0, 0, 0.8);
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
`;

export const OverlayContainer = styled.div`
    display: flex;
    overflow: hidden;
    flex-direction: column;
    position: relative;
    width: fit-content;
    height: fit-content;
    background-color: silver;
    border-radius: 7px;
`;

export const OverlayHeader = styled.div`
    display: flex;
`;

export const OverlayLabel = styled.div`
    display: flex;
    flex: 1;
    justify-content: center;
    align-items: center;
`;

export const OverlayButton = styled.div`
    display: flex;
    background-color: white;
    height: 20px;
    padding: 3px 10px;
    border-radius: 0 5px 0 0;
    cursor: pointer;
    border: solid 2px black;
`;
