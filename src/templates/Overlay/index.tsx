import React from 'react';
import * as ST from './styled';

export interface IOverlay {
  children: JSX.Element,
  onClose: () => void,
  label: string,
}

const Overlay: React.FC<IOverlay> = ({ children, onClose, label }) => {

  return (
    <ST.OverlayWrapper>
      <ST.OverlayContainer>
        <ST.OverlayHeader>
          <ST.OverlayLabel>{label}</ST.OverlayLabel>
          <ST.OverlayButton onClick={onClose}>{'Закрыть'}</ST.OverlayButton>
        </ST.OverlayHeader>
        {children}
      </ST.OverlayContainer>
    </ST.OverlayWrapper>
  );
}

export default Overlay;