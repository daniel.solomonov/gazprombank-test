import styled from 'styled-components';

export const ContentWrapper = styled.div`
    background-color: pink;
    border-radius: 10px;
    padding: 5px;
    text-align: left;
    box-shadow: black 0px 0px 3px;
    display: flex;
    flex-direction: column;
    gap: 7px;
    height: fit-content;
`;
