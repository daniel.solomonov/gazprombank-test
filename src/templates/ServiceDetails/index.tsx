import React from 'react';
import * as ST from './styled';

export interface IDetails {
  content: string,
  id: string,
  name: string,
  price: number,
}

const ServiceDetails: React.FC<IDetails> = ({ content, id, name, price }) => {

  return (
    <ST.ContentWrapper>
      <span>{content}</span>
      <span>{id}</span>
      <span>{name}</span>
      <span>{`${price} ₽`}</span>
    </ST.ContentWrapper>
  );
}

export default ServiceDetails;