import * as ST from './styled';

interface ISelectOption {
  radioHandler: (event: React.ChangeEvent<HTMLSelectElement>) => void,
  variants: string[],
  selected: string,
};

const SelectOption = ({ radioHandler, variants, selected }: ISelectOption) => {
  return (
    <ST.SelectOptionContainer>
      <ST.SelectOptionSelect
        onChange={radioHandler}
        value={selected}
      >
        {
          variants.map(variant => {
            return (<option
              key={variant}
              value={variant}
            >
              {variant}
            </option>);
          })
        }
      </ST.SelectOptionSelect>
    </ST.SelectOptionContainer>
  );
};

export default SelectOption;
