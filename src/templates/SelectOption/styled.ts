import styled from 'styled-components';

export const SelectOptionContainer = styled.div`
    width: fit-content;
    display: flex;
    flex-direction: row;
`;

export const SelectOptionSelect = styled.select`
    padding: 0 7px;
    height: 30px;
    text-align: center;
    font-size: 16px;
    border-radius: 7px;
    border-width: 2px;
    border-style: solid;
    outline: none;
    border-color: black;
    color: black;
    background-color: silver;
`;
