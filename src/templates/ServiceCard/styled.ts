import styled from 'styled-components';

export const ContentWrapper = styled.div`
    background-color: lightblue;
    border-radius: 5px;
    padding: 5px;
    width: 250px;
    text-align: left;
    box-shadow: black 0px 0px 3px;
    display: flex;
    flex-direction: column;
    gap: 5px;
    cursor: pointer;
`;
