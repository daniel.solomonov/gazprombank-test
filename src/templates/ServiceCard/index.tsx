import React, { useCallback } from 'react';
import * as ST from './styled';

export interface IService {
  id: string,
  name: string,
  price: number,
  onClick?: (id: string) => void,
}

const ServiceCard: React.FC<IService> = ({ id, name, price, onClick }) => {

  const onDetail = useCallback(() => {
    if (id && !!onClick) {
      onClick(id);
    }
  }, [id, onClick]);

  return (
    <ST.ContentWrapper onClick={onDetail}>
      <span>{name}</span>
      <span>{`${price} ₽`}</span>
    </ST.ContentWrapper>
  );
}

export default ServiceCard;