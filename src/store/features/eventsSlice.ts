import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

export interface IEvent {
  name: string,
  timeStart: string,
  timeEnd: string,
  timeRemind: number,
};

export interface IDayEvent {
  date: string,
  events: IEvent[],
};

export type EventsState = IDayEvent[];

const initialState: EventsState = [];

export const eventsSlice = createSlice({
  name: 'events',
  initialState,
  reducers: {
    addEvent: (state, action: PayloadAction<{ date: string, event: IEvent }>) => {
      return [
        ...(state.filter(({ date }) => date !== action.payload.date) || []),
        {
          date: action.payload.date,
          events: [
            ...(
              state.find(({ date }) => date === action.payload.date)
                ?.events.filter(obj => obj.timeStart !== action.payload.event.timeStart)
              || []),
            action.payload.event,
          ],
        }
      ];
    },
    removeEvent: (state, action: PayloadAction<{ date: string, timeStart: string }>) => {
      return [
        ...(state.filter(({ date }) => date !== action.payload.date) || []),
        {
          date: action.payload.date,
          events: [
            ...(
              state.find(({ date }) => date === action.payload.date)
                ?.events.filter(obj => obj.timeStart !== action.payload.timeStart)
              || []),
          ],
        }
      ];
    },
  },
});

export const { addEvent, removeEvent } = eventsSlice.actions;

export default eventsSlice.reducer;
